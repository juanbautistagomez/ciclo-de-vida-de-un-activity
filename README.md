# Ciclo de vida de un activity

### El ciclo de vida de un activity se gestiona de la siguiente manera

1. OnCreate
2. onStart
3. onResume
4. onPause
5. onStop
6. onRestart
7. onDestroy

### onCreate

onCreate se refiere cuando inicia la aplicación se crea la actividad por primera vez.

### onStart

onStart ya la aplicacion iniciada entra en estado de ejecución y el usuario visualiza la actividad mientras se prepara para entrar en primer plano y se haga interactiva, este estado o ciclo se completa muy rapido tal como el onCreate es decir la actividad no entra en modo iniciando o cargando, una vez completada este estado en consecutivo la actividad entra en estado onResume

### onResume
onResume es cuando la actividad es interactuable por el usuario la aplicación permanece en este estado hasta que el usuario ejecute un evento que de quite este estado por ejemplo navegar en otra actividad o simplemente el usuario cambia de aplicaion, o el usuario lo cierra entre otros eventos que el usuario aplique

### onPause
Entra en estado onPause cuando la actividad esta siendo abandonada por el usuario, esto no significa que se finaliza si no que estara indicando que la aplicaión ya no se ejecute en primer plano aunque estara visible en modo multiventana 

### onStop
Entra en estado onStop cuando la actividad ya no es visible para el usuari, el sistema también puede llamar a onStop() cuando haya terminado la actividad y esté a punto de finalizar. También debes utilizar onStop() para realizar operaciones de finalización con un uso relativamente intensivo de la CPU. Por ejemplo, si no encuentras un momento más oportuno para guardar información en una base de datos, puedes hacerlo en onStop().

## onDestroy

Se llama a onDestroy() antes de que finalice la actividad. El sistema invoca esta devolución de llamada por los siguientes motivos:

La actividad está terminando (debido a que el usuario la descarta por completo o a que se llama a finish()).
El sistema está finalizando temporalmente la actividad debido a un cambio de configuración (como la rotación del dispositivo o el modo multiventana).
Cuando la actividad pase al estado Destroyed, cualquier componente que priorice el ciclo de vida vinculado al de la actividad recibirá el evento ON_DESTROY. Aquí es donde los componentes del ciclo de vida pueden recuperar cualquier elemento que se necesite antes de que finalice el objeto Activity.

En lugar de poner lógica en ese objeto para determinar por qué está finalizando la actividad, deberías utilizar un objeto ViewModel a fin de contener los datos de vista relevantes para Activity. Si se va a recrear el objeto Activity debido a un cambio de configuración, no es necesario que ViewModel realice ninguna acción, ya que se conservará y se entregará a la siguiente instancia del objeto Activity. Si no se va a recrear el objeto Activity, entonces ViewModel tendrá el método onCleared(), en el que podrá recuperar cualquier dato que necesite antes de que finalice la actividad.

Puedes diferenciar estos dos casos con el método isFinishing().

Si la actividad está terminando, onDestroy() es la devolución de llamada del ciclo de vida final que recibe la actividad. Si se llama a onDestroy() como resultado de un cambio de configuración, el sistema crea inmediatamente una nueva instancia de actividad y luego llama a onCreate() en esa nueva instancia en la nueva configuración.

La devolución de llamada onDestroy() debe liberar todos los recursos que aún no han sido liberados por devoluciones de llamada anteriores, como onStop().

## Observa el siguiente diagrama

![captu](cdv.png)